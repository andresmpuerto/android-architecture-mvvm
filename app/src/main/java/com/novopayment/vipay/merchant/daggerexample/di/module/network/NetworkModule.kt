package com.novopayment.vipay.merchant.daggerexample.di.module.network

import com.novopayment.vipay.merchant.daggerexample.BuildConfig
import com.novopayment.vipay.merchant.daggerexample.MyApp
import com.novopayment.vipay.merchant.daggerexample.data.remote.ApiEndpoints
import com.novopayment.vipay.merchant.daggerexample.utils.BASE_URL_CORE
import com.novopayment.vipay.merchant.daggerexample.utils.Memory
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
open class NetworkModule {

    open fun buildOkHttpClient(app: MyApp): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG)
            HttpLoggingInterceptor.Level.BODY
        else
            HttpLoggingInterceptor.Level.NONE
        return OkHttpClient.Builder()
            .connectTimeout(10L, TimeUnit.SECONDS)
            .writeTimeout(10L, TimeUnit.SECONDS)
            .readTimeout(30L, TimeUnit.SECONDS)
            .cache(
                Cache(
                    File(app.cacheDir, "OkCache"),
                    Memory.calcCacheSize(app, .25f)
                )
            )
            .addInterceptor(interceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(app: MyApp): OkHttpClient = buildOkHttpClient(app)

    /**
     * Provides the Retrofit object.
     * @return the Retrofit object
     */
    @Provides
    @Singleton
    internal fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL_CORE)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }

    /**
     * Provides the Post service implementation.
     * @param retrofit the Retrofit object used to instantiate the service
     * @return the Post service implementation.
     */
    @Provides
    @Singleton
    internal fun provideApiEndpoints(retrofit: Retrofit): ApiEndpoints = retrofit.create(ApiEndpoints::class.java)
}