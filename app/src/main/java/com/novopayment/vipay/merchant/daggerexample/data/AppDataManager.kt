package com.novopayment.vipay.merchant.daggerexample.data

import android.annotation.SuppressLint
import com.novopayment.vipay.merchant.daggerexample.data.model.ResponseTO
import com.novopayment.vipay.merchant.daggerexample.data.remote.ApiEndpoints
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppDataManager @Inject constructor(val api: ApiEndpoints) : DataManager {

    @SuppressLint("CheckResult")
    override fun getUsers(): Observable<Response<ResponseTO>> {
        return api.getUsers(2)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun setUserAsLoggedOut() {
        //TODO("not implemented") //To change body of creatreted functions use File | Settings | File Templates.
    }

    override fun updateApiHeader(userId: Long?, accessToken: String) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateUserInfo(
        accessToken: String,
        userId: Long?,
        loggedInMode: DataManager.LoggedInMode,
        userName: String,
        email: String,
        profilePicPath: String
    ) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}