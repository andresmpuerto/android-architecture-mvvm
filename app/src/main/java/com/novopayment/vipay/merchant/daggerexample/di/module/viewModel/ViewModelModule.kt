package com.novopayment.vipay.merchant.daggerexample.di.module.viewModel

import com.novopayment.vipay.merchant.daggerexample.data.AppDataManager
import com.novopayment.vipay.merchant.daggerexample.data.DataManager
import com.novopayment.vipay.merchant.daggerexample.di.module.general.GeneralModule
import com.novopayment.vipay.merchant.daggerexample.utils.rx.AppSchedulerProvider
import com.novopayment.vipay.merchant.daggerexample.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [GeneralModule::class])
open class ViewModelModule {

    @Provides
    @Singleton
    fun provideDataManager(appDataManager: AppDataManager): DataManager = appDataManager


    @Provides
    fun provideSchedulerProvider(): SchedulerProvider = AppSchedulerProvider()

}