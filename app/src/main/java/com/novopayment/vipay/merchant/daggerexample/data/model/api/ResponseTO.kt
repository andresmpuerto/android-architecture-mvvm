package com.novopayment.vipay.merchant.daggerexample.data.model

import com.novopayment.vipay.merchant.daggerexample.data.model.api.users.User

open class ResponseTO {
    val page : Int = 1
    val per_page: Int = 1
    val total: Int = 1
    val total_pages: Int = 1
    val data: ArrayList<User> = ArrayList()
}