package com.novopayment.vipay.merchant.daggerexample

import android.content.Context
import com.novopayment.vipay.merchant.daggerexample.di.component.DaggerAppComponent
import com.novopayment.vipay.merchant.daggerexample.di.module.network.NetworkModule
import com.novopayment.vipay.merchant.daggerexample.utils.LoggerUtil
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication


open class MyApp : DaggerApplication() {

    lateinit var androidInjector: AndroidInjector<out DaggerApplication>

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        androidInjector = DaggerAppComponent.builder()
            .application(this)
            //.database(databaseModule())
            .network(networkModule())
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        initTimber()
    }

    public override fun applicationInjector(): AndroidInjector<out DaggerApplication> = androidInjector
   // protected open fun databaseModule(): DatabaseModule = DatabaseModule()
    protected open fun networkModule(): NetworkModule = NetworkModule()
    protected open fun initTimber() = LoggerUtil.init()

}
