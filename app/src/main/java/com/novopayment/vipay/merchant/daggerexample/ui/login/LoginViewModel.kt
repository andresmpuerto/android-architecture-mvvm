package com.novopayment.vipay.merchant.daggerexample.ui.login

import com.novopayment.vipay.merchant.daggerexample.data.DataManager
import com.novopayment.vipay.merchant.daggerexample.ui.base.BaseViewModel
import com.novopayment.vipay.merchant.daggerexample.utils.rx.SchedulerProvider

class LoginViewModel (dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<LoginNavigator>(dataManager, schedulerProvider) {

    }