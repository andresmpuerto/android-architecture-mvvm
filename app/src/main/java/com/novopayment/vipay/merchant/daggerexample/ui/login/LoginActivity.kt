package com.novopayment.vipay.merchant.daggerexample.ui.login

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.novopayment.vipay.merchant.daggerexample.BR
import com.novopayment.vipay.merchant.daggerexample.ui.base.BaseActivity
import com.novopayment.vipay.merchant.daggerexample.R
import com.novopayment.vipay.merchant.daggerexample.ViewModelFactory
import com.novopayment.vipay.merchant.daggerexample.databinding.ActivityLoginBinding
import javax.inject.Inject

class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>(), LoginNavigator {

    private var mLoginViewModel: LoginViewModel? = null

    @Inject
    lateinit var factory: ViewModelFactory
    private var mActivityLoginBinding: ActivityLoginBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivityLoginBinding = getViewDataBinding()
        mLoginViewModel?.setNavigator(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_login
    }

    override fun getViewModel(): LoginViewModel {
        mLoginViewModel = ViewModelProviders.of(this, factory)
            .get(LoginViewModel::class.java)
        return mLoginViewModel!!
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

}
