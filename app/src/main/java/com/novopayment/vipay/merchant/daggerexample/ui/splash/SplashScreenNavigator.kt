package com.novopayment.vipay.merchant.daggerexample.ui.splash

interface SplashScreenNavigator {

    fun openLoginActivity()

    fun openMainActivity()
}