package com.novopayment.vipay.merchant.daggerexample.ui.main

import android.annotation.SuppressLint
import android.databinding.ObservableField
import com.novopayment.vipay.merchant.daggerexample.data.DataManager
import com.novopayment.vipay.merchant.daggerexample.data.remote.ApiEndpoints
import com.novopayment.vipay.merchant.daggerexample.ui.base.BaseViewModel
import com.novopayment.vipay.merchant.daggerexample.utils.LoggerUtil
import com.novopayment.vipay.merchant.daggerexample.utils.rx.SchedulerProvider
import javax.inject.Inject


class MainViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<MainNavigator>(dataManager, schedulerProvider) {

    private val NoAction = -1
    val ACTION_ADD_ALL = 0
    val ACTION_DELETE_SINGLE = 1

    @Inject
    lateinit var api: ApiEndpoints

    private val appVersion = ObservableField<String>()

    // private val questionDataList = ObservableArrayList<>()

    private val userEmail = ObservableField<String>()

    private val userName = ObservableField<String>()

    private val imageUrl = ObservableField<String>()

    private var action = NoAction

    fun getAppVersion(): ObservableField<String> = appVersion

    fun getUserEmail(): ObservableField<String> = userEmail

    fun getUserName(): ObservableField<String> = userName

    fun getImageUrl(): ObservableField<String> = imageUrl

    fun updateAppVersion(version: String) = appVersion.set(version)


    fun defaults(){
        imageUrl.set("https://s3.amazonaws.com/uifaces/faces/twitter/marcoramires/128.jpg")
    }

    @SuppressLint("CheckResult")
    fun getUsers() {
        getDataManager().getUsers()
            .subscribe(
                { res ->
                    LoggerUtil.d("TODO")
                    when (res.code()) {
                        200 -> {
                            userName.set(
                                res.body()!!.data[0].first_name + " " + res.body()!!.data[0].last_name
                            )
                            userEmail.set(res.body()!!.data[0].email)
                            imageUrl.set(res.body()!!.data[0].avatar)
                        }
                        400 -> {
                            try {
                                LoggerUtil.e(res.errorBody()!!.string())
                            } catch (e: Exception) {
                                LoggerUtil.e(e.message.toString())
                            }
                        }
                        else -> {
                            LoggerUtil.e("error http")
                        }
                    }
                },
                { error -> LoggerUtil.e(error.message.toString()) })
    }
}