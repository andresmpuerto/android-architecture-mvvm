package com.novopayment.vipay.merchant.daggerexample.data

import com.novopayment.vipay.merchant.daggerexample.data.model.ResponseTO
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import retrofit2.Response

interface DataManager {

    fun setUserAsLoggedOut()

    fun updateApiHeader(userId: Long?, accessToken: String)

    fun updateUserInfo(
        accessToken: String,
        userId: Long?,
        loggedInMode: LoggedInMode,
        userName: String,
        email: String,
        profilePicPath: String
    )

    fun getUsers(): Observable<Response<ResponseTO>>

    enum class LoggedInMode private constructor(val type: Int) {

        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_GOOGLE(1),
        LOGGED_IN_MODE_FB(2),
        LOGGED_IN_MODE_SERVER(3)
    }
}