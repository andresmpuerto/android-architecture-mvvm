package com.novopayment.vipay.merchant.daggerexample.ui.base

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import android.view.inputmethod.InputMethodManager
import com.novopayment.vipay.merchant.daggerexample.utils.NetworkUtils
import dagger.android.AndroidInjection

abstract class BaseActivity <T : ViewDataBinding, V>: AppCompatActivity() {

    private var mViewDataBinding: T? = null
    private var mViewModel: V? = null

    /**
     * @return layout resource id
     */
    @LayoutRes
    abstract fun getLayoutId(): Int

    abstract fun getViewModel(): V

    abstract fun getBindingVariable(): Int

    //---
    override fun onCreate(savedInstanceState: Bundle?) {
        performDependencyInjection()
        super.onCreate(savedInstanceState)
        performDataBinding()
    }

    fun getViewDataBinding(): T {
        return mViewDataBinding!!
    }

    fun isNetworkConnected(): Boolean {
        return NetworkUtils.isNetworkConnected(applicationContext)
    }

    private fun performDependencyInjection() {
        AndroidInjection.inject(this)
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun showLoading() {
        //TODO
    }

    fun hideLoading() {
        //TODO
    }


    fun showHomeDisplay() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    //---
    fun setTitleToolbar(title: String) {
        supportActionBar?.title = title
    }

    fun setTitleToolbar(id: Int) {
        supportActionBar?.title = getString(id)
    }

    private fun performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId())
        this.mViewModel = mViewModel ?: getViewModel()
        mViewDataBinding?.setVariable(getBindingVariable(), mViewModel)
        mViewDataBinding?.executePendingBindings()
    }


}
