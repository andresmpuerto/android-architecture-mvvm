package com.novopayment.vipay.merchant.daggerexample.data.remote

import javax.inject.Inject
//import javax.inject.Singleton


class ApiEndpointsImpl constructor(private val api: ApiEndpoints){

    fun getUsers(page: Int) = api.getUsers(page)


}