package com.novopayment.vipay.merchant.daggerexample.di.builder

import com.novopayment.vipay.merchant.daggerexample.ui.login.LoginActivity
import com.novopayment.vipay.merchant.daggerexample.ui.main.MainActivity
import com.novopayment.vipay.merchant.daggerexample.ui.splash.SplashScreenActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class ActivityBuilder {

    @ContributesAndroidInjector //(modules = [MainModule::class])
    abstract fun contributeMainInjector(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindSplashActivity(): SplashScreenActivity

    @ContributesAndroidInjector
    abstract fun bindLoginActivity(): LoginActivity

}