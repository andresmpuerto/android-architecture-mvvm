package com.novopayment.vipay.merchant.daggerexample.ui.splash

import com.novopayment.vipay.merchant.daggerexample.data.DataManager
import com.novopayment.vipay.merchant.daggerexample.ui.base.BaseViewModel
import com.novopayment.vipay.merchant.daggerexample.utils.rx.SchedulerProvider
import java.util.*

class SplashScreenViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<SplashScreenNavigator>(dataManager, schedulerProvider) {

    fun start(){
        Timer().schedule(object : TimerTask() {
            override fun run() {
                getNavigator()?.openLoginActivity()
            }
        }, 4000)
    }

}