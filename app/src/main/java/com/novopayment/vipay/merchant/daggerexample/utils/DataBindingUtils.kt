package com.novopayment.vipay.merchant.daggerexample.utils

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide


object DataBindingUtils {

   // companion object {
        @BindingAdapter("imageUrl")
        @JvmStatic
        fun setImageUrl(imageView: ImageView, url: String) {
            val context = imageView.context
            Glide.with(context).load(url).into(imageView)
        }
 //   }
}
