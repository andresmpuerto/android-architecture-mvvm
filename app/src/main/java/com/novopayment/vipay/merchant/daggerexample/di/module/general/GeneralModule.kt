package com.novopayment.vipay.merchant.daggerexample.di.module.general

import android.content.Context
import com.novopayment.vipay.merchant.daggerexample.data.remote.ApiEndpoints
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
interface GeneralModule {
    //@Binds
    //fun bindPlanetRepository(repository: PlanetRepository): Planet


}