package com.novopayment.vipay.merchant.daggerexample.data.model.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import io.reactivex.annotations.NonNull


@Entity(tableName = "users")
class UserEntity {

    @ColumnInfo(name = "created_at")
    var createdAt: String? = null

    @PrimaryKey
    var id: Long? = null

    var name: String? = null

    @NonNull
    var email: String? = null

    var picture: String? = null

    @ColumnInfo(name = "updated_at")
    var updatedAt: String? = null

}