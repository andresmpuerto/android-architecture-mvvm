package com.novopayment.vipay.merchant.daggerexample.data.remote

import com.novopayment.vipay.merchant.daggerexample.data.model.ResponseTO
import com.novopayment.vipay.merchant.daggerexample.data.model.api.users.ResponseUser
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiEndpoints {

    @GET("/api/users")
    fun getUsers(@Query("page") page: Int): Observable<Response<ResponseTO>>

    @GET("/users/{id}")
    fun getUser(@Path("id") id: Int): Observable<Response<ResponseUser>>
}