package com.novopayment.vipay.merchant.daggerexample.ui.splash

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import com.novopayment.vipay.merchant.daggerexample.BR
import com.novopayment.vipay.merchant.daggerexample.R
import com.novopayment.vipay.merchant.daggerexample.ViewModelFactory
import com.novopayment.vipay.merchant.daggerexample.databinding.ActivitySplashScreenBinding
import com.novopayment.vipay.merchant.daggerexample.ui.base.BaseActivity
import com.novopayment.vipay.merchant.daggerexample.ui.login.LoginActivity
import com.novopayment.vipay.merchant.daggerexample.ui.main.MainActivity
import javax.inject.Inject

class SplashScreenActivity : BaseActivity<ActivitySplashScreenBinding, SplashScreenViewModel>(), SplashScreenNavigator {

    private var mSplashViewModel: SplashScreenViewModel? = null

    @Inject
    lateinit var factory: ViewModelFactory

    override fun getLayoutId(): Int {
        return R.layout.activity_splash_screen
    }

    override fun getViewModel(): SplashScreenViewModel {
        mSplashViewModel = ViewModelProviders.of(this, factory)
            .get(SplashScreenViewModel::class.java)
        return mSplashViewModel!!
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mSplashViewModel?.setNavigator(this)
        //obtener la data inicial
        mSplashViewModel?.start()
    }

    override fun openLoginActivity() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun openMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}