package com.novopayment.vipay.merchant.daggerexample.di.component

import com.bumptech.glide.module.AppGlideModule
import com.novopayment.vipay.merchant.daggerexample.MyApp
import com.novopayment.vipay.merchant.daggerexample.di.builder.ActivityBuilder
import com.novopayment.vipay.merchant.daggerexample.di.module.AppModule
import com.novopayment.vipay.merchant.daggerexample.di.module.network.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class, ActivityBuilder::class, AppModule::class])
interface AppComponent : AndroidInjector<MyApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: MyApp): Builder
        //fun database(database: DatabaseModule): Builder
        fun network(network: NetworkModule): Builder
        fun build(): AppComponent
    }

    override fun inject(app: MyApp)
    fun inject(glideModule: AppGlideModule)
}