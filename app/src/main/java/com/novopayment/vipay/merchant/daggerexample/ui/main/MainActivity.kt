package com.novopayment.vipay.merchant.daggerexample.ui.main

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import com.novopayment.vipay.merchant.daggerexample.databinding.ActivityMainBinding
import android.view.View
import android.support.v7.widget.Toolbar
import com.novopayment.vipay.merchant.daggerexample.BR
import com.novopayment.vipay.merchant.daggerexample.BuildConfig
import com.novopayment.vipay.merchant.daggerexample.R
import com.novopayment.vipay.merchant.daggerexample.ViewModelFactory
import com.novopayment.vipay.merchant.daggerexample.databinding.NavHeaderMainaBinding
import com.novopayment.vipay.merchant.daggerexample.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(), MainNavigator {

    private var mMainViewModel: MainViewModel? = null

    @Inject
    lateinit var factory: ViewModelFactory

    private var mActivityMainBinding: ActivityMainBinding? = null
    private var mDrawer: DrawerLayout? = null
    private var mNavigationView: NavigationView? = null
    private var mToolbar: Toolbar? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun getViewModel(): MainViewModel {
        mMainViewModel = ViewModelProviders.of(this, factory).get(MainViewModel::class.java)
        return mMainViewModel!!
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivityMainBinding = getViewDataBinding()
        mMainViewModel?.setNavigator(this)
        setUp()
    }

    private fun setUp() {
        mDrawer = mActivityMainBinding?.drawerLayout
        mToolbar = mActivityMainBinding?.toolbar
        mNavigationView = mActivityMainBinding?.navView

        setSupportActionBar(mToolbar)
        val mDrawerToggle = object : ActionBarDrawerToggle(
            this, mDrawer, mToolbar,
            R.string.navigation_drawer_open, R.string.navigation_drawer_close
        ) {
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                hideKeyboard()
            }
        }
        mDrawer?.addDrawerListener(mDrawerToggle)
        mDrawerToggle.syncState()
        setupNavMenu()
        val version = "v. " + BuildConfig.VERSION_NAME
        mMainViewModel?.updateAppVersion(version)
        mMainViewModel?.defaults()
        // subscribeToLiveData()
        mMainViewModel?.getUsers()
    }

    private fun setupNavMenu() {
        val navHeaderMainBinding: NavHeaderMainaBinding = DataBindingUtil.inflate(layoutInflater,
            R.layout.nav_header_maina, mActivityMainBinding?.navView, false)

        mActivityMainBinding?.navView?.addHeaderView(navHeaderMainBinding.root)
        navHeaderMainBinding.viewModel = mMainViewModel

        mNavigationView?.setNavigationItemSelectedListener {
            mDrawer?.closeDrawer(GravityCompat.START)
            true
//            when (item.itemId) {
//                R.id.navItemAbout -> {
//                    showAboutFragment()
//                    return@mNavigationView.setNavigationItemSelectedListener true
//                }
//                else -> return@mNavigationView.setNavigationItemSelectedListener
//                false
//            }
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
}
