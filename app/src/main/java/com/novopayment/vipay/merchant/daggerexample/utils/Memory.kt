package com.novopayment.vipay.merchant.daggerexample.utils

import android.app.ActivityManager
import android.content.Context
import android.content.Context.ACTIVITY_SERVICE
import android.content.pm.ApplicationInfo.FLAG_LARGE_HEAP


/**
 * Created by Wasabeef on 2017/10/16.
 */
object Memory {

    fun calcCacheSize(context: Context, size: Float): Long {
        val am = context.getSystemService(ACTIVITY_SERVICE) as ActivityManager
        val largeHeap = context.applicationInfo.flags and FLAG_LARGE_HEAP != 0
        val memoryClass = if (largeHeap) am.largeMemoryClass else am.memoryClass
        return (memoryClass * 1024L * 1024L * size).toLong()
    }
}