package com.novopayment.vipay.merchant.daggerexample.di.module

import com.novopayment.vipay.merchant.daggerexample.di.module.general.GeneralModule
import com.novopayment.vipay.merchant.daggerexample.di.module.network.NetworkModule
import dagger.Module
import com.novopayment.vipay.merchant.daggerexample.di.module.viewModel.ViewModelModule

@Module(includes = [GeneralModule::class, NetworkModule::class, ViewModelModule::class])
internal object AppModule {
    // If you need.

}