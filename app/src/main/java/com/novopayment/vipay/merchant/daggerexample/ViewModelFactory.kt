package com.novopayment.vipay.merchant.daggerexample


import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.novopayment.vipay.merchant.daggerexample.data.DataManager
import com.novopayment.vipay.merchant.daggerexample.ui.login.LoginViewModel
import com.novopayment.vipay.merchant.daggerexample.ui.main.MainViewModel
import com.novopayment.vipay.merchant.daggerexample.ui.splash.SplashScreenViewModel
import com.novopayment.vipay.merchant.daggerexample.utils.rx.SchedulerProvider
import javax.inject.Inject

class ViewModelFactory() : ViewModelProvider.Factory {

    private var dataManager: DataManager? = null
    private var schedulerProvider: SchedulerProvider? = null

    @Inject
    constructor (dataManager: DataManager, schedulerProvider: SchedulerProvider): this() {
        this.dataManager = dataManager
        this.schedulerProvider = schedulerProvider
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(SplashScreenViewModel::class.java) ->
                SplashScreenViewModel(dataManager!!, schedulerProvider!!) as T

            modelClass.isAssignableFrom(MainViewModel::class.java) ->
                MainViewModel(dataManager!!, schedulerProvider!!) as T

            modelClass.isAssignableFrom(LoginViewModel::class.java) ->
                LoginViewModel(dataManager!!, schedulerProvider!!) as T

            else ->
                throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
        }

    }
}