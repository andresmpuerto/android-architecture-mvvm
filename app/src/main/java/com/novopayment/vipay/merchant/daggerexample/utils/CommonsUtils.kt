package com.novopayment.vipay.merchant.daggerexample.utils

import android.util.Patterns

class CommonsUtils {

    companion object {
        //Validaciones

        fun isEmailValid(email: String): Boolean {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

        fun isPhoneValid(phone: String): Boolean {
            return Patterns.PHONE.matcher(phone).matches()
        }
    }
}