package com.novopayment.vipay.merchant.daggerexample.ui.base

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import com.novopayment.vipay.merchant.daggerexample.data.DataManager
import com.novopayment.vipay.merchant.daggerexample.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import java.lang.ref.WeakReference

abstract class BaseViewModel<N>(dataManager: DataManager, schedulerProvider: SchedulerProvider) : ViewModel() {

    private var mDataManager: DataManager? = dataManager
    private var mSchedulerProvider: SchedulerProvider? = schedulerProvider

    private val mIsLoading = ObservableBoolean()
    private var mCompositeDisposable: CompositeDisposable = CompositeDisposable()

    private var mNavigator: WeakReference<N>? = null

    override fun onCleared() {
        mCompositeDisposable.dispose()
        super.onCleared()
    }

    fun getCompositeDisposable(): CompositeDisposable {
        return mCompositeDisposable
    }

    fun getIsLoading(): ObservableBoolean {
        return mIsLoading
    }

    fun setIsLoading(isLoading: Boolean) {
        mIsLoading.set(isLoading)
    }

    fun getNavigator(): N? {
        return mNavigator?.get()
    }

    fun setNavigator(navigator: N) {
        this.mNavigator = WeakReference(navigator)
    }

    fun getDataManager(): DataManager {
        return mDataManager!!
    }

    fun getSchedulerProvider(): SchedulerProvider {
        return mSchedulerProvider!!
    }
}