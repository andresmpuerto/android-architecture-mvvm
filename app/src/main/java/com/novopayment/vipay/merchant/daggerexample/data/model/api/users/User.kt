package com.novopayment.vipay.merchant.daggerexample.data.model.api.users

open class User {
    val id: Int = 0
    val first_name: String = ""
    val last_name: String = ""
    val avatar: String = ""
    val email: String = ""

}